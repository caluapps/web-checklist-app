const eMsgEmptyInput = 'Please type in your todo';

// remove Button
let removeButton = $('<span></span>').text('x').addClass('removeButton');
$('#cUList > li').append(removeButton);

// remove Button's behaviour
$('.removeButton').on({
  mouseenter: function() {
    $(this).addClass('hoverRemoveButton');
  },
  mouseleave: function() {
    $(this).removeClass('hoverRemoveButton');
  },
  click: function() {
    $(this).parent().remove();
  }
});

// Event Handler for Add-Button
$('#cAddButton').on('click', function() {
  let inputValue = $('#cChecklistInput').val();

  if (inputValue) {
    createChecklistItem({text: inputValue});

  } else {
    alert(eMsgEmptyInput);
    console.log('Error:', eMsgEmptyInput);
  }
});

// add Button's behaviour
$('#cAddButton').on({
  mouseover: function() {
    $(this).addClass('addButtonHover');
  },
  mouseleave: function() {
    $(this).removeClass('addButtonHover');
  }
});

// checklist's behaviour
$('#cUList').children().on({
  click: function() {
    $(this).toggleClass('checkedListItem');
    $('img', this).toggleClass('checked');
  }
});


$('#cRemoveRemoveButton').on('click', function() {
  $('span').remove('.removeButton');
});

// Handler create Checklist Item
function createChecklistItem(todo) {
  console.log('todo', todo);
  let listItem = $('<li></li>').text(todo.text);

  $('#cUList').append(listItem);

  $('span').remove('.removeButton');
  $('#cUList li').append(removeButton);
};

// Handler LoadData
function clearListAndLoadData(data) {
  console.log('data:', data);
  $('#cUList').empty();

  let checklistArr = data.todos;

  checklistArr.forEach(function(checkListItem, index, checkList) {
    createChecklistItem(checkListItem);
  });
};

$(document).ready(function() {
  // Fetching Data
  $('#cLoadData').on('click', function() {
    let url = 'https://thawing-springs-31284.herokuapp.com/todos';
    let url2 = 'https://cors-anywhere.herokuapp.com/https://thawing-springs-31284.herokuapp.com/todos';

    // Fetching Data via jQuery.ajax
    $.ajax({
      url: url2,
      type: 'GET',
      contentType: 'application/json; charset=utf-8',
      success: function(resultData) {
        // console.log('resultData', resultData);

        clearListAndLoadData(resultData);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log('jqXHR', jqXHR);
        console.log('textStatus', textStatus);
        console.log('errorThrown', errorThrown);
      },
      timeout: 10000,
    });
  

  /* jQuery cross domain ajax
  $.get(url2).done(function (data) {
      console.log(data);
  }); */

  /* using XMLHttpRequest
  var xhr = new XMLHttpRequest();
  xhr.open("GET", url2, true);
  xhr.onload = function () {
    console.log(xhr.responseText);
  };
  xhr.send(); */

  /* using the Fetch API
  fetch(url)
    .then(function (response) {
      return response.json();
  }).then(function (json) {
      console.log(json);
  }); */

  /* Login Form
  $('#cLoginForm').on('submit', function(e) {
    e.preventDefault();
    let user = {
      email: $('#cEmailInput').val(),
      password: $('#cPasswordInput').val()
    }
    console.log(user);

    // Code Snippet
    // UDEMY Nodejs - https://www.udemy.com/the-complete-nodejs-developer-course-2/learn/v4/questions/4149654
    // Fetching Data
    let data = toJSONString(this);
    console.log(data);
    
    // https://thawing-springs-31284.herokuapp.com/todos
    fetch('https://fierce-wave-97937.herokuapp.com/users', {
        headers: {'content-type': 'application/json'},
        method: 'POST',
        body: data
      })
      .then((res) => res.json())
      .then((res) => console.log(res))
      .catch((err) => console.log(err)) */

    /* Code Snippet from OSA
    // Register User
    let user = {
                email: document.getElementById('email').value,
                password: document.getElementById('password').value,
                name: document.getElementById('name').value
              };

    let request = new XMLHttpRequest();
    request.onreadystatechange = bePrepaired;

    request.open('POST', 'https://calu.lupus.uberspace.de/nodejs/trainingHistory/users', true);
    request.setRequestHeader('Content-Type', 'application/json');
    request.send(JSON.stringify(user));
    */
  });
});